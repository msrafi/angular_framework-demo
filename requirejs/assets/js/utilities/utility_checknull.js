define(function(require) {
    'use strict';

    //utilities
    var isNull = function(elem){ var _v = elem.val();
        if( _v === '' || _v === 'null' || _v === undefined ) {
            return true 
        }else{
            return false
        }
    };

    return isNull;

});