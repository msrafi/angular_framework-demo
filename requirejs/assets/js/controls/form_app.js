require(['form_module', 'modules/form_extend', 'modules/form_overwrite', 'utilities/utility_checknull','modules/rafi_return'], 
		function(FormModule, FormExtend, FormOverwrite, checkNull, myName) { 

			 console.log(FormModule);
			 console.log(FormExtend);
			 console.log(FormOverwrite);
			 console.log(myName);
	 	// START APP1 ==> Using the existing the Form Check 
	 	/* *****************************************************************/
		var $form1      = $('form.form1');
		var form_module = Object.create(FormModule);
	    console.log(form_module);

		form_module.event1($form1, function(form, elem, e){
			localFromFuncton1(form, elem, e);
		});

		/* *****************************************************************/
	 	// END APP1 ==> Using the existing the Form Check 





	 	// START APP2 ==> Using the existing the Form Check 
	 	/* *****************************************************************/
		var $form2      = $('form.form2');
		var form_extend = Object.create(FormExtend);
	    console.log(form_extend);

	    form_extend.event1($form2, function(form, elem, e){
			// Some DUmmy function
			localFromFuncton1(form, elem, e);
		});

		form_extend.event2($form2, function(form, elem, e){
			// Some DUmmy function
			console.log(form.attr('name'), elem.val());
		});

		/* *****************************************************************/
	 	// END APP2 ==> Using the existing the Form Check 





	 	// START APP3 ==> Using the existing the Form Check 
	 	/* *****************************************************************/
		var $form3         = $('form.form3');
		var form_overwrite = Object.create(FormOverwrite);
	    console.log(form_overwrite);
	    

	    form_overwrite.event1($form3, function(form, elem, e){
			// Some DUmmy function
			localFromFuncton1(form, elem, e);
		});

		form_overwrite.event2($form3, function(form, elem, e){
			// Some DUmmy function
			localFormFunction2(form, elem, e);
		});

		/* *****************************************************************/
	 	// END APP3 ==> Using the existing the Form Check 





	 	// App based functions
	 	function localFromFuncton1(form, elem, e){
	 		
	 		// Some DUmmy function
			var _node 		= elem.get(0).nodeName.toLowerCase(), 
			_elemtype     	= elem.get(0).type,
			_elemName		= elem.attr('name');

			if((_elemtype === 'radio' || _node === 'select') && e === 'change'){
				console.log(form.attr('name'), elem.attr('name'), elem.val());
			};
			// Some DUmmy function

	 	};


	 	// App based functions				
	 	function localFormFunction2(form, elem, e){

	 		var _node 		= elem.get(0).nodeName.toLowerCase(), 
			_elemtype     	= elem.get(0).type,
			_elemName		= elem.attr('name');

	 		// More control over the elements and events
			if(_node === 'input' && _elemtype === 'text' && e === 'keyup'){

				if(checkNull(elem)){
					elem.css('border', '1px solid red')
				}else{
					elem.css('border', '1px solid green')
				}
				console.log(form.attr('name'), elem.attr('name'), elem.val());
			};
	 	}


 	// var FormCheck2 = require('form_check2');


 	// console.log(FormExtend, FormOverwrite);

 });
