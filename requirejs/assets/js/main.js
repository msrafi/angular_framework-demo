require.config({

    baseUrl: './assets/js/',
    paths: {
        'Modernizr': 'vendors/modernizr',
        'jquery': 'vendors/jquery.min',
        'jquery-ui': 'vendors/jquery-ui.min',
        'form_module': 'modules/form_module'
    },
    shim: {
        'Modernizr': {
            exports: 'Modernizr'
        }
        // Add here if something is not AMD
    },
    deps: ['require'],

    callback: function(require) {
        'use strict';


        if (typeof Object.create !== 'function') { //polyfill for object.create
            Object.create = function(obj) {
                function F() {};
                F.prototype = obj;
                return new F();
            };
        };

        // Call all module apps here
        require(['controls/form_app']);

    }
});
