define(function(require) {
    'use strict';

    var $ = require('jquery');
    var FormExtend = require('modules/form_extend');

    // Overwriting the objects
    var FormOverwrite = {
    	event2: function(obj, callback){var _ = this;
            _.$obj = obj;

            _.$obj.each(function(){var _t = $(this);
                _t.find('input, button, select').on('keyup', function(e){ var _tt = $(this);
                    return callback(_t, _tt, e.type);
                });
            });
        }
    };

    return FormOverwrite = $.extend({}, FormExtend, FormOverwrite);

});