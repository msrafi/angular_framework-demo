define(function(require) {
    'use strict';

    var $ = require('jquery');
    var FormModule = require('form_module');

    // Extending Form Module
    var FormExtend = {
    	event2: function(obj, callback){var _ = this;
    		_.$obj = obj;

			_.$obj.each(function(){var _t = $(this);
                _t.find('input').on('blur', function(e){ var _tt = $(this);
                    return callback(_t, _tt, e.type);
                });
            });
    	}
    };

    return FormExtend = $.extend({}, FormModule, FormExtend);

});