define(function(require) {
    'use strict';

    // We need jQuery
    var $ = require('jquery');


    // Our code form Module
    var FormModule = {
    	event1: function(obj, callback){var _ = this;
    		_.$obj = obj;

			_.$obj.each(function(){var _t = $(this);
				_t.find('input, select').on('change', function(e){ var _tt = $(this);
					return callback(_t, _tt, e.type);
				});
			});
    	}
    };


    return FormModule;

});