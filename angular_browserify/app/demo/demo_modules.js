
'use strict';

var angDemo = angular.module('demo',[])
	.directive('halfwidth', function(){
		return{
			scope: true,
			restrict: 'A',
			replace: true,
			transclude: true,
			link: function(s, e, a){
				s.colorclass = a.colorclass;
				s.btnlbl     = a.btnlbl;
				s.column     = a.column;
				s.eyebrow    = a.eyebrow;
				s.heading    = a.heading;
				s.linking    = a.linking;
			},
			templateUrl: 'app/demo/demo_partials.html'
		}
	})

return angDemo;