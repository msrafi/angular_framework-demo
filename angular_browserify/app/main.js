'use strict';

var angular = require('angular');
var $ = require('jquery');

require('../app/demo/demo_modules');

var app = angular.module('demoApp', ['demo']);
