'use strict';

var browserify = require('browserify')
  , clean = require('gulp-clean')
  , glob = require('glob')
  , gulp = require('gulp')
  , source = require('vinyl-source-stream');

gulp.task('bfy', function() {
  return browserify('./app/main.js')
  .bundle()
  .pipe(source('app.js'))
  .pipe(gulp.dest('./app/dist/'));
});

gulp.task('watch', function() {
  gulp.watch(['app/**/*.js'], ['bfy']);
});