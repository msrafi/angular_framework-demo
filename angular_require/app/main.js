// var demoApp = angular.module('demoApp', []);
require.config({
    baseUrl: 'app/',
    paths: {
        'angular': '../assets/libs/angular/angular.min',
        'jquery': '../assets/libs/jquery/jquery.min',
        'bootstrap': '../assets/libs/bootstrap/dist/js/bootstrap.min'
    },
    shim: {
        angular: {
            exports: 'angular'
        },
        bootstrap: {
            deps: ['jquery']
        }
    },
    deps: ['app']
});

