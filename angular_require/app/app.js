define(['require', 'jquery','../app/_demo/demo_modules'], function(require, $) {
    'use strict';

    
    var angular = require('angular');

    // Start a new module called 'demoApp'
    // 'demo' is the name of the module that we required through 'demo_modules' 
    var app = angular.module('demoApp', ['demo']);

    // Angular Bootstrap, kind of functiona.init()
    $(function() {
        angular.bootstrap(document, ['demoApp']);
    });

    // Returning the app
    return app;
});

