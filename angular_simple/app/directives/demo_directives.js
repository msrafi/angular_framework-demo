demoApp.directive('halfwidth', function(){
		return{
			scope: true,
			restrict: 'A',
			replace: true,
			transclude: true,
			link: function(s, e, a){
				console.log(s, e, a);
				s.colorclass = a.colorclass || 'blue';
				s.btnlbl     = a.btnlbl;
				s.column     = a.column;
				s.eyebrow    = a.eyebrow;
				s.heading    = a.heading;
				s.linking    = a.linking;
			},
			templateUrl: 'app/partials/demo_partials.html'
		}
	});
// demoApp.directive('car', function(){
// 	return{
// 		scope: true,
// 		restrict: 'E',
// 		link: function(scope, element, attrs){
// 			scope.model = attrs.model;
// 		},
// 		controller: 'carController',
// 		templateUrl: 'app/partials/demo_partials.html'
// 	}
// }).
// directive('wheels', function(){
// 	return{
// 		restrict: 'A',
// 		require: 'car',
// 		link: function(scope, element, attrs, carController){
// 			carController.addFeature('Has wheels');
// 		}
// 	}
// }).
// directive('brakes', function(){
// 	return{
// 		restrict: 'A',
// 		require: 'car',
// 		link: function(scope, element, attrs, carController){
// 			carController.addFeature('Has Brakes');
// 		}
// 	}
// })
