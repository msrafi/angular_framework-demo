
angular.module('demo',[])
	.controller('carController', function ($scope){
		$scope.features = ['It is a Car'];
		this.addFeature = function(f){$scope.features.push(f)};
	})
	.directive('car', function(){
		return{
			scope: true,
			restrict: 'E',
			link: function(scope, element, attrs){
				scope.model = attrs.model;
			},
			controller: 'carController',
			templateUrl: 'app/partials/demo_partials.html'
		}
	}).
	directive('wheels', function(){
		return{
			restrict: 'A',
			require: 'car',
			link: function(scope, element, attrs, carController){
				carController.addFeature('Has wheels');
			}
		}
	}).
	directive('brakes', function(){
		return{
			restrict: 'A',
			require: 'car',
			link: function(scope, element, attrs, carController){
				carController.addFeature('Has Brakes');
			}
		}
	})